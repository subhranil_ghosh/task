# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Demo application for SWEDBANK.

### How do I get set up? ###

Steps to run the Application. 

•	Import the spring boot project in STS.
•	Project ->Maven-> Maven update. 
•	Run as Spring Boot app.
•	If using command prompt please run the respective commands to run previous two steps.
•	Import the postman collection in POSTMAN. 
•	Hit the GET request getRouteRequestDetails.
•	You can change connId value to test the API. 
•	If using browser , open chrome and paste the url http://localhost:8080/getRouteRequestDetails?connId=01d503272c0b40ba. Change the connId value to test

log.txt file (zipped), Task App.postman_collection.json are in utils folder of the repository.

Note : 
•	Please mention the log.txt absolute path in application.properties as
file.path = C:\\Users\\subhranil.ghosh\\Desktop\\logs\\log.txt (Replace the path according to your log file absolute location)

Feel free to go through code and provide feedback if any. 😊