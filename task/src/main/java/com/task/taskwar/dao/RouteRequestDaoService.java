package com.task.taskwar.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import com.task.taskwar.pojo.RouteRequest;
import com.task.taskwar.pojo.RouteRequestRowMapper;

@Service
public class RouteRequestDaoService {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private String query = "SELECT * FROM ROUTE_REQUEST_ENTITY WHERE ATTRIBUTE_CONN_ID = ?";
	
		@SuppressWarnings("deprecation")
		public RouteRequest getRouteRequest(@RequestParam(name = "connId") String connId){
			RouteRequest request = jdbcTemplate.queryForObject(query, new Object[] { connId }, new RouteRequestRowMapper());
			return request;
		}
}
