package com.task.taskwar;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.task.taskwar.constant.AttributeNameConstant;
import com.task.taskwar.controller.RouteRequestController;
import com.task.taskwar.dao.RouteRequestDaoService;
import com.task.taskwar.entity.RouteRequestEntity;
import com.task.taskwar.exceptions.RouteRequestException;
import com.task.taskwar.repository.service.RouteRequestRepository;

@SpringBootApplication
@ComponentScan(basePackageClasses  = {RouteRequestController.class,RouteRequestDaoService.class})
public class TaskApplication {
	
	@Autowired
	private RouteRequestRepository repository;
	
	@Value("${file.path}")
	private String path;
	
	@Value("${file.message}")
	private String message;
	
	public static final String splitRegex = "\\s+";
	 
	Map<String,String> map = new HashMap<String,String>();
	
	public static final int TOTAL_NUMBER_OF_ATTRIBUTES = 24;
	

	public static void main(String[] args){
		   SpringApplication.run(TaskApplication.class, args);
		}
		
	@PostConstruct
	private void saveDataToInMemoryDB() throws RouteRequestException {
			
			Reader reader = null;
			/*Read file from destination*/
			try {
					reader = new FileReader(path);
					
			} catch (FileNotFoundException e) {
				
				throw new RouteRequestException(e.getMessage());
			}  
			
			/*Scanning the log file*/
			LineIterator it = IOUtils.lineIterator(reader);
			   
			   while (it.hasNext()) {
			     String line = it.nextLine();
			     if(line.contains(message)) {
			    	 int lineNumber = 0;
			    	 while(lineNumber<TOTAL_NUMBER_OF_ATTRIBUTES) {
			    		 String attribute = it.nextLine();
			    		 addDataToMap(attribute, map, 0);
			    		 lineNumber++;
			    	 }
			    	 	
			    	 	    RouteRequestEntity dbData = new RouteRequestEntity(map.get(AttributeNameConstant.AttributeConnID), 
						   		map.get(AttributeNameConstant.AttributeDNIS), map.get(AttributeNameConstant.AttributeANI), map.get(AttributeNameConstant.Call_ID), map.get(AttributeNameConstant.P_Asserted_Identity),  
								map.get(AttributeNameConstant.AttributeCallUUID), map.get(AttributeNameConstant.original_dialplan_digits));
						   		
			    	 		/*Save Data to in-memory database(H2db)*/
			    	 		repository.save(dbData);
					 
			     }
			 }
		}
	
	/*Method to Map attributes as key-value pairs*/
	private Map<String, String> addDataToMap(String line, Map<String, String> dataMap, int initialDataIndex){
		String[] attributeKeyValuesPairs = line.strip().split(splitRegex);
		dataMap.put(attributeKeyValuesPairs[initialDataIndex], attributeKeyValuesPairs[initialDataIndex+1]);
		return dataMap;
	}
}
	
