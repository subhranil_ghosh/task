package com.task.taskwar.exceptions;

public class RouteRequestException extends Exception{
	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;
	
	public String errorMsg;

	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	public RouteRequestException(String errorMsg) {
		super();
		this.errorMsg = errorMsg;
	}
}
