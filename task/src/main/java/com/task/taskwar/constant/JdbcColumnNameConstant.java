package com.task.taskwar.constant;

public class JdbcColumnNameConstant {
	
	public JdbcColumnNameConstant() {};
	
	public static final String ATTRIBUTE_CONN_ID = "ATTRIBUTE_CONN_ID";
	public static final String ATTRIBUTE_DNIS = "ATTRIBUTE_DNIS";
	public static final String ATTRIBUTE_ANI = "ATTRIBUTE_ANI";
	public static final String CALL_ID = "CALL_ID";
	public static final String ATTRIBUTE_CALL_UUID = "ATTRIBUTE_CALL_UUID";
	public static final String P_ASSERTED_IDENTITY = "P_ASSERTED_IDENTITY";
	public static final String ORIGINAL_DIAL_PLAN_DIGIT = "ORIGINAL_DIAL_PLAN_DIGIT";

}
