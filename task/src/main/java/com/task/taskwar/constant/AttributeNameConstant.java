package com.task.taskwar.constant;

public class AttributeNameConstant {
	
	public AttributeNameConstant() {};
	
	public static final String AttributeConnID = "AttributeConnID";
	public static final String AttributeDNIS = "AttributeDNIS";
	public static final String AttributeANI = "AttributeANI";
	public static final String Call_ID = "'Call-ID'";
	public static final String P_Asserted_Identity = "'P-Asserted-Identity'";
	public static final String AttributeCallUUID = "AttributeCallUUID";
	public static final String original_dialplan_digits = "'original-dialplan-digits'";

}
