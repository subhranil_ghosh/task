package com.task.taskwar.repository.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.task.taskwar.entity.RouteRequestEntity;

@Repository
public interface RouteRequestRepository  extends CrudRepository<RouteRequestEntity, String>{}
