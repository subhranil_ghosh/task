package com.task.taskwar.pojo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.task.taskwar.constant.JdbcColumnNameConstant;


public class RouteRequestRowMapper implements RowMapper<RouteRequest>{
	
	    
	    public RouteRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
			RouteRequest request = new RouteRequest();
			
			
			request.setAttributeConnID(rs.getString(JdbcColumnNameConstant.ATTRIBUTE_CONN_ID));
			request.setAttributeDNIS(rs.getString(JdbcColumnNameConstant.ATTRIBUTE_DNIS));
			request.setAttributeANI(rs.getString(JdbcColumnNameConstant.ATTRIBUTE_ANI));
			request.setCallId(rs.getString(JdbcColumnNameConstant.CALL_ID));
			request.setPassertedIdentity(rs.getString(JdbcColumnNameConstant.P_ASSERTED_IDENTITY));
			request.setAttributeCallUUID(rs.getString(JdbcColumnNameConstant.ATTRIBUTE_CALL_UUID));
			request.setOriginalDialplanDigit(rs.getString(JdbcColumnNameConstant.ORIGINAL_DIAL_PLAN_DIGIT));
	        
			return request;
	    }
}
