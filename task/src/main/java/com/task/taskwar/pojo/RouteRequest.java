package com.task.taskwar.pojo;

import java.io.Serializable;

public class RouteRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RouteRequest() {};

	private String attributeConnID;

	private String attributeDNIS;
	
	private String attributeANI;
	
	private String callId; 
	
	private String passertedIdentity;
	
	private String attributeCallUUID;
	
	private String originalDialplanDigit;

	public String getAttributeConnID() {
		return attributeConnID;
	}

	public void setAttributeConnID(String attributeConnID) {
		this.attributeConnID = attributeConnID;
	}

	public String getAttributeDNIS() {
		return attributeDNIS;
	}

	public void setAttributeDNIS(String attributeDNIS) {
		this.attributeDNIS = attributeDNIS;
	}

	public String getAttributeANI() {
		return attributeANI;
	}

	public void setAttributeANI(String attributeANI) {
		this.attributeANI = attributeANI;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public String getPassertedIdentity() {
		return passertedIdentity;
	}

	public void setPassertedIdentity(String passertedIdentity) {
		this.passertedIdentity = passertedIdentity;
	}

	public String getAttributeCallUUID() {
		return attributeCallUUID;
	}

	public void setAttributeCallUUID(String attributeCallUUID) {
		this.attributeCallUUID = attributeCallUUID;
	}

	public String getOriginalDialplanDigit() {
		return originalDialplanDigit;
	}

	public void setOriginalDialplanDigit(String originalDialplanDigit) {
		this.originalDialplanDigit = originalDialplanDigit;
	}

	public RouteRequest(String attributeConnID, String attributeDNIS, String attributeANI, String callId,
			String passertedIdentity, String attributeCallUUID, String originalDialplanDigit) {
		super();
		this.attributeConnID = attributeConnID;
		this.attributeDNIS = attributeDNIS;
		this.attributeANI = attributeANI;
		this.callId = callId;
		this.passertedIdentity = passertedIdentity;
		this.attributeCallUUID = attributeCallUUID;
		this.originalDialplanDigit = originalDialplanDigit;
	}
}
