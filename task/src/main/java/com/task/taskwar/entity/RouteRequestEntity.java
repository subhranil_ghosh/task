package com.task.taskwar.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class RouteRequestEntity {
	
	@Id
	@Column(name = "ATTRIBUTE_CONN_ID")
	private String attributeConnID;

	@Column(name = "ATTRIBUTE_DNIS")
	private String attributeDNIS;
	
	@Column(name = "ATTRIBUTE_ANI") 
	private String attribute_ANI;
	
	@Column(name = "CALL_ID")
	private String callId; 
	
	@Column(name = "P_ASSERTED_IDENTITY")
	private String passertedIdentity;
	
	@Column(name = "ATTRIBUTE_CALL_UUID")
	private String attributeCallUUID;
	
	@Column(name = "ORIGINAL_DIAL_PLAN_DIGIT")
	private String originalDialplanDigit;
	
	public RouteRequestEntity() {};

	public String getAttributeConnID() {
		return attributeConnID;
	}

	public void setAttributeConnID(String attributeConnID) {
		this.attributeConnID = attributeConnID;
	}

	public String getAttributeDNIS() {
		return attributeDNIS;
	}

	public void setAttributeDNIS(String attributeDNIS) {
		this.attributeDNIS = attributeDNIS;
	}

	public String getAttributeANI() {
		return attribute_ANI;
	}

	public void setAttributeANI(String attributeANI) {
		this.attribute_ANI = attributeANI;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public String getPassertedIdentity() {
		return passertedIdentity;
	}

	public void setPassertedIdentity(String passertedIdentity) {
		this.passertedIdentity = passertedIdentity;
	}

	public String getAttributeCallUUID() {
		return attributeCallUUID;
	}

	public void setAttributeCallUUID(String attributeCallUUID) {
		this.attributeCallUUID = attributeCallUUID;
	}

	public String getOriginalDialplanDigit() {
		return originalDialplanDigit;
	}

	public void setOriginalDialplanDigit(String originalDialplanDigit) {
		this.originalDialplanDigit = originalDialplanDigit;
	}


	public RouteRequestEntity(String attributeConnID, String attributeDNIS, String attributeANI, String callId,
			String passertedIdentity, String attributeCallUUID, String originalDialplanDigit) {
		super();
		this.attributeConnID = attributeConnID;
		this.attributeDNIS = attributeDNIS;
		this.attribute_ANI = attributeANI;
		this.callId = callId;
		this.passertedIdentity = passertedIdentity;
		this.attributeCallUUID = attributeCallUUID;
		this.originalDialplanDigit = originalDialplanDigit;
	}

}
