package com.task.taskwar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.task.taskwar.dao.RouteRequestDaoService;
import com.task.taskwar.pojo.RouteRequest;

@CrossOrigin("*")
@RestController
public class RouteRequestController {
	
	@Autowired
	RouteRequestDaoService dao;
	
	@GetMapping("/getRouteRequestDetails")
	public RouteRequest getRouteRequest(@RequestParam(name = "connId") String connId){
		return dao.getRouteRequest(connId);
	}
}
